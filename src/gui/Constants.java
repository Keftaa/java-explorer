package gui;

public class Constants {
	public static int WIDTH = 800;
	public static int HEIGHT = 640;
	public static int HALF_WIDTH = WIDTH/2;
	public static int HALF_HEIGHT = HEIGHT/2;
	public final static String APP_PATH = System.getProperty("user.dir");
}
