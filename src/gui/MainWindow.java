package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Label;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;

public class MainWindow extends JFrame {
	JPanel root, topPanel, middlePanel, bottomPanel;
	GridBagLayout topPanelLayout, middlePanelLayout;
	Label title;
	JButton editButton, copyButton, moveButton, deleteButton, searchButton, createFolderButton;
	JList partitionsList, partitionContentList, shutUpList;
	JButton byNameButton, byExtensionButton, byDateButton, bySizeButton;
	
	public void render(){
		root = new JPanel();
		topPanel = new JPanel();
		topPanel.setBackground(Color.BLACK);
		topPanelLayout = new GridBagLayout();
		topPanel.setLayout(topPanelLayout);
		title = new Label("Java Explorer");
		title.setFont(new Font("Arial", Font.BOLD, 24));
		
		editButton = new JButton("Edit");
		copyButton = new JButton("Copy");
		moveButton = new JButton("Move");
		deleteButton = new JButton("Delete");
		searchButton = new JButton("Search");
		createFolderButton = new JButton("Create Folder");
		
		topPanel.add(title, setRow(0));
		topPanel.add(editButton, setPosition(0, 1));
		topPanel.add(copyButton, setPosition(1, 1));
		topPanel.add(moveButton, setPosition(2, 1));
		topPanel.add(deleteButton, setPosition(3, 1));
		topPanel.add(searchButton, setPosition(4, 1));
		topPanel.add(createFolderButton, setPosition(5, 1));
		
		
		
		middlePanel = new JPanel();
		middlePanel.setBackground(Color.GRAY);
		middlePanelLayout = new GridBagLayout();
		middlePanel.setLayout(middlePanelLayout);
		
		String[] data = {"A", "B", "C"};
		String[] data2 = {"A", "B", "C", "D", "E", "D", "D", "D", "D", "D", "D"};
		partitionsList = new JList<String>(data);
		partitionsList.setFixedCellWidth(160);
		partitionContentList = new JList<String>(data2);
		partitionContentList.setFixedCellWidth(270);
		shutUpList = new JList<String>(data);
		shutUpList.setFixedCellWidth(320);
		byNameButton = new JButton("Sort by Name");
		middlePanel.add(partitionsList, setPosition(0, 0));
		middlePanel.add(partitionContentList, setPosition(1, 0));
		middlePanel.add(shutUpList, setPosition(2, 0));
		middlePanel.add(byNameButton, setPosition(2, 1, GridBagConstraints.ABOVE_BASELINE));
	
		//root.setLayout(new BoxLayout(root, BoxLayout.Y_AXIS));
		root.add(topPanel);
		root.add(middlePanel);
		this.add(root);
		this.setSize(Constants.WIDTH, Constants.HEIGHT);
		this.setResizable(false);
		this.setVisible(true);
		
	}

	public GridBagConstraints setPosition(int x, int y){
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = x;
		gbc.gridy = y;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.anchor = GridBagConstraints.BASELINE;
		gbc.insets = new Insets(5, 5, 5, 5);
		return gbc;
	}

	public GridBagConstraints setPosition(int x, int y, int anchor){
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = x;
		gbc.gridy = y;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.anchor = anchor;
		gbc.insets = new Insets(5, 5, 5, 5);
		return gbc;
	}
	
	public GridBagConstraints setRow(int row){
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridy = row;
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbc.anchor = GridBagConstraints.CENTER;
		return gbc;
	}
	
	public GridBagConstraints setColumn(int col){
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = col;
		gbc.gridy = 0;
		gbc.gridheight = GridBagConstraints.REMAINDER;
		gbc.anchor = GridBagConstraints.NORTH;
		gbc.insets = new Insets(5, 5, 5, 5); 
		return gbc;
	}
	
	public static void main (String [] args){
		new MainWindow().render();
	}
	
}
