package models;

import javax.swing.table.AbstractTableModel;


public class MyTableModel extends AbstractTableModel{
	private static final long serialVersionUID = 1L;
	private String[] columnNames;	
	private Object[][] data;
	

	
	
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return getValueAt(0, columnIndex).getClass();
	}

	@Override
	public String getColumnName(int column) {
		return columnNames[column];
	}

	@Override
	public int getColumnCount() {
		if(columnNames==null) return 0;
		return columnNames.length;
	}

	@Override
	public int getRowCount() {
		if(data==null) return 0;
		return data.length;
	}

	@Override
	public Object getValueAt(int row, int col) {
		if(data==null) return null;
		return data[row][col];
	}

	public String[] getColumnNames() {
		return columnNames;
	}

	public void setColumnNames(String[] columnNames) {
		this.columnNames = columnNames;
	}

	public Object[][] getData() {
		return data;
	}

	public void setData(Object[][] data) {
		this.data = data;
	}
	
	public void truncateData(){
		this.data = null;
	}
	
}