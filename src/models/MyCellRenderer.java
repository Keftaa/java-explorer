package models;

import java.awt.Color;
import java.awt.Component;
import java.io.File;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.filechooser.FileSystemView;

@SuppressWarnings("rawtypes")
public class MyCellRenderer extends JLabel implements ListCellRenderer {

	private static final long serialVersionUID = 1L;
	FileSystemView fileSystemView;

	public MyCellRenderer() {
		fileSystemView = FileSystemView.getFileSystemView();
	}

	@Override
	public Component getListCellRendererComponent(JList arg0, Object arg1,
			int arg2, boolean arg3, boolean arg4) {
		// TODO Auto-generated method stub
		JLabel label = new JLabel();
		label.setOpaque(true);
		File file = (File) arg1;
		label.setIcon(fileSystemView.getSystemIcon(file));
		label.setText(fileSystemView.getSystemDisplayName(file));
		if (arg3) {
			label.setBackground(Color.BLACK);
			label.setForeground(Color.BLUE);
		} else {
			label.setBackground(Color.WHITE);
			label.setForeground(Color.BLACK);
		}
		return label;
	}
}