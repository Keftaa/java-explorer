package linkers;

import java.io.File;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableRowSorter;

import json.JsonHelper;
import managers.FilesListManager;
import models.MyTableModel;
import views.View;

public class FilesTableLinker implements Linker, TableModelListener, Observer {
	private JTable table;
	private MyTableModel model;
	private boolean isLocked;
	private Object[][] data;
	private FilesListManager manager;
	private List<Observable> observables;
	
	public FilesTableLinker(View view, List<Observable> observables){
		this.observables = observables;
		table = (JTable) view.getMainComponent();
		model = JsonHelper.getTable();
		table.setModel(model);
		table.setRowSorter(new TableRowSorter<>(model));
		manager = new FilesListManager();
		File file = manager.getMainDirectory();
		if(file!=null);
		data = new Object[file.listFiles().length][4];
		updateModelData(file);
		isLocked = true;
	}
	
	private void updateModelData(File file){
		File[] files = manager.getFilesListIn(file);
		model.truncateData();
		data = new Object[files.length][4];
		for(int i = 0; i < files.length; i++){
			for(int j = 0; j < model.getColumnCount(); j++){
				switch(model.getColumnName(j)){
				case "Nom fichier":
					data[i][j] = files[i];
					break;
				case "Taille":
					data[i][j] = files[i].length();
					break;
				case "Date":
					data[i][j] = files[i].lastModified();
					break;
				case "File/Dir":
					data[i][j] = files[i].isDirectory();
					break;
				}
			}
		}
		model.setData(data);
		model.fireTableDataChanged();
	}
	
	@Override
	public void init() {
		isLocked = false;
	}

	@Override
	public void reset() {
		manager = new FilesListManager();
		
	}

	@Override
	public void disable() {
		isLocked = true;
		
	}

	@Override
	public void tableChanged(TableModelEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(Observable arg0, Object currentFile) {
		if(observables.contains(arg0) && currentFile instanceof File){
			System.out.println(arg0);
			updateModelData(((File) currentFile));
			System.out.println("Files table linker notified");
		}
		
		
	}

}