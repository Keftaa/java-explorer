package linkers;

import java.io.File;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import managers.FilesListManager;
import views.View;

public class RootsListLinker extends Observable implements Linker, ListSelectionListener {
	private JList<File> rootsList;
	private DefaultListModel<File> listModel;
	private FilesListManager listManager;
	private boolean isLocked;

	
	@SuppressWarnings("unchecked")
	public RootsListLinker(View listPanel){
		listManager = new FilesListManager();
		rootsList = (JList<File>) listPanel.getMainComponent();
		listModel = new DefaultListModel<File>();
		rootsList.setModel(listModel);
		rootsList.addListSelectionListener(this);
		updateModel(listManager.getMainDirectory());
		isLocked = true;
	}
	
	private void updateModel(File file){
		if(isLocked) return;
		
		File[] files = listManager.getRoots();
		listModel.clear();
		for(File f: files)
			listModel.addElement(f);
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		if(isLocked) return;
		
		setChanged();
		notifyObservers(rootsList.getSelectedValue());
	}

	@Override
	public void init() {
		setChanged();
		notifyObservers(true);
		isLocked = false;
		
	}

	@Override
	public void reset() {
		listManager = new FilesListManager();
		
	}

	@Override
	public void disable() {
		// TODO Auto-generated method stub
		
	}
	
}
