package linkers;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class LinkersManager implements Observer {
	
	private ArrayList<Observable> observables;
	
	public LinkersManager(){
		observables = new ArrayList<Observable>();
	}
	
	public ArrayList<Observable> getObservables(){
		return observables;
	}
	
	
	@Override
	public void update(Observable o, Object initialization) {
		if(initialization instanceof Boolean && (boolean) initialization){
			observables.add(o);
			System.out.println("Observables started: "+observables.size());
		}
		
	}

}
