package linkers;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Observable;
import java.util.Observer;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

import managers.RenameManager;
import views.View;

public class RenameLinker extends Observable implements Observer, Linker,
		ActionListener {

	private JButton renameButton;
	private JList<File> filesList;
	private RenameManager renameManager;
	// private FilesListManager filesListManager;
	private JScrollPane targetListPane;
	private boolean isLocked;

	@SuppressWarnings("unchecked")
	public RenameLinker(View panel, View targetPanel) {
		renameManager = new RenameManager();
		renameButton = (JButton) panel.getComponentByName("Rename");
		renameButton.addActionListener(this);
		renameManager.addObserver(this);
		targetListPane = (JScrollPane) targetPanel
				.getComponentByName("filesListPane");
		filesList = (JList<File>) targetListPane.getViewport().getView();
		// filesListManager = new FilesListManager();
		isLocked = true;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (isLocked || filesList.isSelectionEmpty())
			return;

		java.util.List<File> selectedFiles = filesList.getSelectedValuesList();
		renameManager.setFilesToRename(selectedFiles);
		renameManager.setNewName(JOptionPane.showInputDialog(new JFrame(),
				"Enter new name", null));
		renameManager.start();
		renameButton.setText(renameManager.getState());
		renameManager.waitUntilDone();
		reset();
		setChanged();
		notifyObservers(filesList.getSelectedValue());

	}

	@Override
	public void reset() {
		renameManager = new RenameManager();
		renameManager.addObserver(this);
	}

	@Override
	public void init() {
		setChanged();
		notifyObservers(true);
		isLocked = false;
	}

	@Override
	public void disable() {
		isLocked = true;

	}

	@Override
	public void update(Observable arg0, Object arg1) {
		System.out.println("Rename conflict observer notified !");
		if (arg1 instanceof File[]) {
			File[] conflictFiles = (File[]) arg1;
			EventQueue.invokeLater(new Runnable() {

				@Override
				public void run() {
					String newName = JOptionPane.showInputDialog(
							"This name already exists", "otherName");
					renameManager.setResolution(newName, conflictFiles[1]);
					renameManager.waitUntilDone();
					System.out.println("done bro");
					setChanged();
					filesList.setSelectedValue(conflictFiles[1], true);
					notifyObservers(filesList.getSelectedValue());
				}

			});
			

		}
	}

}
