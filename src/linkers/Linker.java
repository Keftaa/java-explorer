package linkers;

public interface Linker {
	public void init();
	public void reset();
	public void disable();
}
