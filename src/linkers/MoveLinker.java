package linkers;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

import managers.MoveManager;
import views.View;

public class MoveLinker extends Observable implements Observer, ActionListener, Linker {
	private JButton moveButton;
	private JList<File> filesList;
	private MoveManager moveManager;
	private int clickCount;
	private boolean isLocked;

	@SuppressWarnings("unchecked")
	public MoveLinker(View panel, View targetPanel) {
		moveManager = new MoveManager();
		moveManager.addObserver(this);
		moveButton = (JButton) panel.getComponentByName("Move");
		moveButton.addActionListener(this);
		JScrollPane targetListPane = (JScrollPane) targetPanel
				.getComponentByName("filesListPane");
		filesList = (JList<File>) targetListPane.getViewport().getView();
		clickCount = 0;
		isLocked = true;

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(isLocked) return;
		
		clickCount++;

		switch (clickCount) {
		case 2:
			moveManager.setDestination(filesList.getSelectedValue());
			moveManager.start();

		default:
			moveManager.joinThread();
			setChanged();
			notifyObservers(filesList.getSelectedValue());
			reset();
			break;

		case 1:
			moveManager.setFilesToMove(filesList.getSelectedValuesList());
			break;
		}
		moveButton.setText(moveManager.getState());

	}

	@Override
	public void reset() {
		clickCount = 0;
		moveManager = new MoveManager();
		moveManager.addObserver(this);

	}

	@Override
	public void init() {
		setChanged();
		notifyObservers(true);
		isLocked = false;

	}

	@Override
	public void disable() {
		isLocked = true;
		
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		System.out.println("Move conflict observer notified !");
		if (arg1 instanceof File[]) {
			File[] conflictData = (File[]) arg1;
			EventQueue.invokeLater(new Runnable() {

				@Override
				public void run() {
					String newName = JOptionPane.showInputDialog(
							"This destination already exists", "otherName");
					moveManager.setResolution(newName, conflictData[0], conflictData[1]);
					moveManager.waitUntilDone();
					setChanged();
					filesList.setSelectedValue(conflictData[0], true);
					notifyObservers(filesList.getSelectedValue());
				}

			});
			

		}
		
	}

}
