package linkers;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import managers.FilesListManager;
import views.View;

public class FilesListLinker extends Observable implements Linker, Observer, ListSelectionListener, MouseListener {
	private JList<File> filesList;
	private DefaultListModel<File> listModel;
	private FilesListManager listManager;
	private List<Observable> observables;
	private boolean isLocked;
	
	@SuppressWarnings("unchecked")
	public FilesListLinker(View listPanel, List<Observable> observables){
		this.observables = observables;
		listManager = new FilesListManager();
		filesList = (JList<File>) listPanel.getMainComponent();
		listModel = new DefaultListModel<File>();
		filesList.setModel(listModel);
		filesList.addListSelectionListener(this);
		filesList.addMouseListener(this);
		updateModel(listManager.getMainDirectory());
		isLocked = true;
	}
	
	private void updateModel(File file){
		
		File[] files = listManager.getFilesListIn(file);
		listModel.clear();
		if(files!=null)
		for(File f: files)
			listModel.addElement(f);
		setChanged();
		notifyObservers(file);
	}
	
	@Override
	public void update(Observable arg0, Object currentFile) {
		if(isLocked) return;
		
		if(observables.contains(arg0) && currentFile instanceof File){
			updateModel(((File) currentFile).getParentFile());
			System.out.println("Files list linker notified");
		}
		else if(observables.contains(arg0))
			System.out.println("wtf");
		
		
	}
	public void mouseClicked(MouseEvent arg0) {	}
	public void mouseEntered(MouseEvent arg0) {}
	public void mouseExited(MouseEvent arg0) {}
	public void mousePressed(MouseEvent arg0) {}
	public void mouseReleased(MouseEvent arg0) {}

	@Override
	public void init() {
		isLocked = false;
		setChanged();
		notifyObservers(true);
	}

	@Override
	public void reset() {
		listManager = new FilesListManager();
		
	}

	@Override
	public void disable() {
		isLocked = true;
		
	}

}
