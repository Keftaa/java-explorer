package linkers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Observable;

import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JScrollPane;

import managers.DeleteManager;
import views.View;

public class DeleteLinker extends Observable implements ActionListener, Linker {
	private JButton deleteButton;
	private JList<File> filesList;
	private DeleteManager deleteManager;
	private boolean isLocked;

	@SuppressWarnings("unchecked")
	public DeleteLinker(View panel, View targetPanel) {
		deleteManager = new DeleteManager();
		deleteButton = (JButton) panel.getComponentByName("Delete");
		deleteButton.addActionListener(this);
		JScrollPane targetListPane = (JScrollPane) targetPanel
				.getComponentByName("filesListPane");
		filesList = (JList<File>) targetListPane.getViewport().getView();
		isLocked = true;

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(isLocked) return;
		
		File reference = filesList.getSelectedValue();
		deleteManager.setFilesToDelete(filesList.getSelectedValuesList());
		deleteManager.start();
		deleteButton.setText(deleteManager.getState());
		deleteManager.waitUntilDone();
		reset();
		setChanged();
		notifyObservers(reference);
	}

	@Override
	public void reset() {
		deleteManager = new DeleteManager();
	}

	@Override
	public void init() {
		setChanged();
		notifyObservers(true);
		isLocked = false;

	}

	@Override
	public void disable() {
		isLocked = true;
		
	}

}
