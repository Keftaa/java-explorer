package linkers;

import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.List;

import javax.swing.JFrame;

import managers.SizeManager;
import views.View;

public class SizeLinker implements ComponentListener, Linker {
	private JFrame frame;
	private SizeManager sizeManager;
	private boolean isLocked;
	private List<View> views;
	
	public SizeLinker(View mainView, List<View> views){
		this.views = views;
		frame = (JFrame) mainView.getMainComponent();
		frame.addComponentListener(this);
		sizeManager = new SizeManager();
		isLocked = true;
	}
	@Override
	public void componentHidden(ComponentEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void componentMoved(ComponentEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void componentResized(ComponentEvent arg0) {
		if(isLocked) return;
		sizeManager.setWidth(frame.getBounds().width);
		sizeManager.setHeight(frame.getBounds().height);
		sizeManager.start();
		for(View v: views)
			v.updateDefaultSize();

	}

	@Override
	public void componentShown(ComponentEvent arg0) {
		// TODO Auto-generated method stub

	}
	@Override
	public void init() {
		isLocked = false;
		
	}
	@Override
	public void reset() {
		sizeManager = new SizeManager();
		
	}
	@Override
	public void disable() {
		isLocked = true;
		
	}

}
