package linkers;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

import managers.CopyManager;
import views.View;

public class CopyLinker extends Observable implements Observer, ActionListener, Linker {
	private JList<File> targetList;
	private int clickCount;
	private List<File> filesToCopy;
	private File destination;
	private JButton copyButton;
	private CopyManager copyManager;
	private boolean isLocked;

	@SuppressWarnings("unchecked")
	public CopyLinker(View panel, View centerPanel) {
		clickCount = 0;
		JScrollPane targetListPane = (JScrollPane) centerPanel
				.getComponentByName("filesListPane");
		targetList = (JList<File>) targetListPane.getViewport().getView();
		filesToCopy = new ArrayList<File>();
		copyButton = (JButton) panel.getComponentByName("Copy");
		copyButton.addActionListener(this);
		copyManager = new CopyManager();
		copyManager.addObserver(this);
		isLocked = true;
	}

	public void init() {
		setChanged();
		notifyObservers(true);
		isLocked = false;
	}
	


	public void reset() {
		filesToCopy = new ArrayList<File>();
		copyManager = new CopyManager();
		copyManager.addObserver(this);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(isLocked) return;
		
		clickCount++;

		switch (clickCount) {
		case 2:
			copyManager.setDestination(targetList.getSelectedValue());
			copyManager.start();

		default:
			copyManager.waitUntilDone();
			setChanged();
			notifyObservers(targetList.getSelectedValue());
			reset();
			break;

		case 1:
			copyManager.setFilesToCopy(targetList.getSelectedValuesList());
			break;
		}
		copyButton.setText(copyManager.getState());

	}

	@Override
	public void update(Observable arg0, Object arg1) {
		System.out.println("Copy conflict observer notified !");
		if (arg1 instanceof File[]) {
			File[] conflictData = (File[]) arg1;
			EventQueue.invokeLater(new Runnable() {

				@Override
				public void run() {
					String newName = JOptionPane.showInputDialog(
							"This destination already exists", "otherName");
					copyManager.setResolution(newName, conflictData[0], conflictData[1]);
					copyManager.waitUntilDone();
					setChanged();
					targetList.setSelectedValue(targetList.getModel().getElementAt(0), true);
					notifyObservers(targetList.getSelectedValue());
				}

			});
			

		}
		
	}

	@Override
	public void disable() {
		isLocked = true;
		
	}
}
