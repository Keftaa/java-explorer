package managers;

import gui.Constants;

public class SizeManager implements Manager {

	private int width, height;
	
	public SizeManager(){
		width = 800;
		height = 640;
	}

	public float getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	@Override
	public void waitUntilDone() {}

	@Override
	public String getState() {
		return null;
	}

	@Override
	public void start() {
		Constants.WIDTH = width;
		Constants.HEIGHT = height;
	}

	@Override
	public void start(int priority) {}
}
