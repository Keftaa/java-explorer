package managers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

public class DeleteManager implements Runnable, Manager {
	private Thread thread;
	private List<File> filesToDelete;

	public DeleteManager(List<File> filesToDelete) {
		this.filesToDelete = filesToDelete;

	}
	
	public DeleteManager(){
		
	}
	
	public void setFilesToDelete(List<File> files){
		this.filesToDelete = files;
	}

	/**
	 * 
	 */
	public boolean delete() {
		boolean result;
		for(File file: filesToDelete){
			if(!delete(file)){
				System.out.println("The fuck bro ? I can't delete that shit.");
				return false; // return false is one delete operation fails
			}
		}
		return true;
	}
	
	private boolean delete(File file){
		if(file.isDirectory()){
			System.out.println("It's a directory !");
			delete(file.listFiles());
		}
		System.out.println("It's a file!");
		return file.delete();
	}
	
	private void delete(File[] files){
		for(File file: files){
			if(file.isDirectory()){
				System.out.println("It's a directory !!");
				delete(file.listFiles());
			}
			System.out.println("Deleting a file.");
			delete(file);
				
		}
	}

	String name;

	public void start() {
		if (thread == null) {
			thread = new Thread(this);
			thread.start();
		}
	}

	/**
	 * 
	 * @param priority
	 *            10 maximum, 1 minimum
	 */
	public void start(int priority) {
		if (thread == null) {
			thread = new Thread(this);
			thread.setPriority(priority);
			thread.start();
		}
	}

	@Override
	public void run() {
		delete();
	}

	public String getState() {
		return "Delete";
	}

	@Override
	public void waitUntilDone() {
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}

}
