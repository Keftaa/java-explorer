package managers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Observable;

public class MoveManager extends Observable implements Runnable, Manager {
	private Thread thread;
	private List<File> filesToMove;
	private boolean overwrite;
	private File destination;

	public MoveManager(List<File> filesToMove, File destination,
			boolean overwrite) {
		this.overwrite = overwrite;
		this.filesToMove = filesToMove;
		this.destination = destination;

	}

	public MoveManager() {

	}

	public void setFilesToMove(List<File> files) {
		this.filesToMove = files;
	}

	public void setDestination(File destination) {
		this.destination = destination;
	}

	public void setOverwrite(boolean doOverwrite) {
		this.overwrite = doOverwrite;
	}

	/**
	 * 
	 */
	public boolean moveStoredToDestination() {
		boolean toDirectory = destination.isDirectory();

		if (overwrite)
			for (File file : filesToMove)
				try {
					Files.move(file.toPath(),
							toDirectory ? new File(destination.toString()
									+ File.separator + file.getName()).toPath()
									: destination.toPath(),
							StandardCopyOption.REPLACE_EXISTING);
				} catch (IOException e) {
					e.printStackTrace();
					return false;
				}
		else {
			if (toDirectory) {
				File targetFile = null;
				for (File file : filesToMove) {
					try {
						targetFile = new File(destination.toString()
								+ File.separator + file.getName());
						if (targetFile.exists()) {
							System.out.println("CONFLICT !");
							setConflict(targetFile, file);
							continue;
						}
						Files.move(file.toPath(),
								new File(destination.toString()
										+ File.separator + file.getName())
										.toPath());
					} catch (IOException e) {
						e.printStackTrace();
						return false;
					}
				}
			} else {
				for (File file : filesToMove) {

					try {
						Files.move(file.toPath(), new File(destination.toString()
								+ File.separator + file.getName()).toPath());
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}

		return true;
	}

	public void start() {
		if (thread == null) {
			thread = new Thread(this);
			thread.start();
		}
	}

	/**
	 * 
	 * @param priority
	 *            10 maximum, 1 minimum
	 */
	public void start(int priority) {
		if (thread == null) {
			thread = new Thread(this);
			thread.setPriority(priority);
			thread.start();
		}
	}

	@Override
	public void run() {
		moveStoredToDestination();
	}

	public String getState() {
		if (filesToMove == null)
			return "Move";
		return "Paste";
	}

	public void joinThread() {
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void setConflict(File existingFile, File fileToMove) {
		File[] conflictData = {existingFile, fileToMove};
		setChanged();
		notifyObservers(conflictData);
		System.out.println("Notifying observer of move conflict !");
	}

	public void setResolution(String newName, File destination,
			File fileToMove) {
		File newDestinationFile = new File(destination.getParent()+File.separator+newName);
		resolve(newDestinationFile, fileToMove);
	}

	private void resolve(File newDestination, File fileToMove) {
		if(newDestination.exists())
			setConflict(newDestination, fileToMove);
		else{
			try {
				Files.move(fileToMove.toPath(), newDestination.toPath());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void waitUntilDone() {
		try {
			if (thread != null)
				thread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
		}

	}
	
	

}
