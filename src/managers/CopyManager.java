package managers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Observable;

public class CopyManager extends Observable implements Runnable, Manager {
	private Thread thread;
	private List<File> filesToCopy;
	private boolean overwrite;
	private File destination;

	public CopyManager(List<File> filesToCopy, File destination,
			boolean overwrite) {
		this.overwrite = overwrite;
		this.filesToCopy = filesToCopy;
		this.destination = destination;

	}

	public CopyManager() {

	}

	public void setFilesToCopy(List<File> files) {
		this.filesToCopy = files;
	}

	public void setDestination(File destination) {
		this.destination = destination;
	}

	public void setOverwrite(boolean doOverwrite) {
		this.overwrite = doOverwrite;
	}

	/**
	 * 
	 */
	public boolean copyStoredToDestination() {
		boolean toDirectory = destination.isDirectory();

		if (overwrite)
			for (File file : filesToCopy)
				try {
					System.out.println("destination: "
							+ (toDirectory ? new File(destination.toString()
									+ File.separator + file.getName()).toPath()
									: destination));
					Files.copy(file.toPath(),
							toDirectory ? new File(destination.toString()
									+ File.separator + file.getName()).toPath()
									: destination.toPath(),
							StandardCopyOption.REPLACE_EXISTING);
				} catch (IOException e) {
					e.printStackTrace();
					return false;
				}
		else {
			if (toDirectory) {
				File targetFile = null;
				for (File file : filesToCopy)
					try {
						targetFile = new File(destination.toString()
								+ File.separator + file.getName());
						if (targetFile.exists()) {
							System.out.println("CONFLICT !");
							setConflict(targetFile, file);
							continue;
						}
						Files.copy(file.toPath(),
								new File(destination.toString()
										+ File.separator + file.getName())
										.toPath());
					} catch (IOException e) {
						e.printStackTrace();
						return false;
					}
			} else {
				for (File file : filesToCopy) {

					try {
						Files.copy(file.toPath(),
								new File(destination.toString()
										+ File.separator + file.getName())
										.toPath());
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return true;
	}

	String name;

	public void start() {
		if (thread == null) {
			thread = new Thread(this);
			thread.start();
		}
	}

	/**
	 * 
	 * @param priority
	 *            10 maximum, 1 minimum
	 */
	public void start(int priority) {
		if (thread == null) {
			thread = new Thread(this);
			thread.setPriority(priority);
			thread.start();
		}
	}

	@Override
	public void run() {
		copyStoredToDestination();
	}

	public String getState() {
		if (filesToCopy == null)
			return "Copy";
		return "Paste";
	}

	@Override
	public void waitUntilDone() {
		try {
			if(thread!=null)
				thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

	private void setConflict(File existingFile, File fileToCopy) {
		File[] conflictData = { existingFile, fileToCopy };
		setChanged();
		notifyObservers(conflictData);
		System.out.println("Notifying observer of copy conflict !");
	}

	public void setResolution(String newName, File destination, File fileToCopy) {
		File newDestinationFile = new File(destination.getParent()
				+ File.separator + newName);
		resolve(newDestinationFile, fileToCopy);
	}

	private void resolve(File newDestination, File fileToCopy) {
		if (newDestination.exists())
			setConflict(newDestination, fileToCopy);
		else {
			try {
				Files.copy(fileToCopy.toPath(), newDestination.toPath());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
