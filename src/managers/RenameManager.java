package managers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Observable;

public class RenameManager extends Observable implements Runnable, Manager {
	private List<File> filesToRename;
	private String newName;
	private Thread thread;

	public RenameManager(List<File> filesToRename, String newName) {
		this.filesToRename = filesToRename;
		this.newName = newName;
	}

	public RenameManager() {

	}

	public void setFilesToRename(List<File> files) {
		this.filesToRename = files;
	}

	public void setNewName(String newName) {
		this.newName = newName;
	}

	private boolean renameStored() {
		String suffix = "";
		int i = 0;
		File targetFile = null;
		for (File file : filesToRename) {
			suffix = "_"+i;
			try {
				targetFile = new File(file.getParentFile()+File.separator+newName
						+ (i == 0 ? "" : suffix));
				if(targetFile.exists()){
					System.out.println("CONFLICT !");
					setConflict(targetFile, file);
					continue;
					//thread.join();
					
				}
				else
					Files.move(file.toPath(), targetFile.toPath());
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("Rename failed.");
				return false;
			}
			i++;
		}
		return true;
	}

	private void setConflict(File targetFile, File fileToRename) {
		File[] files = {targetFile, fileToRename};
		setChanged();
		notifyObservers(files);
		System.out.println("Notifying observer of rename conflict !");
	}
	
	public void setResolution(String newName, File fileToRename){
		System.out.println("Resolution set !");
		File newNameFile = new File(fileToRename.getParent()+File.separator+newName);
		resolve(newNameFile, fileToRename);
	}
	
	private void resolve(File newNameFile, File fileToRename){
		if(newNameFile.exists()){
			setConflict(newNameFile, fileToRename);
			System.out.println("does");
		}
		else{
			System.out.println("doesn't");
			try {
				Files.move(fileToRename.toPath(), newNameFile.toPath());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void run() {
		renameStored();
	}

	public void start() {
		if (thread == null) {
			thread = new Thread(this);
			thread.setPriority(7);
			thread.start();
		}
	}

	@Override
	public void waitUntilDone() {
		try {
			if(thread!=null)
				thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

	@Override
	public String getState() {
		return "Rename";
	}

	@Override
	public void start(int priority) {
		if (thread == null) {
			thread = new Thread(this);
			thread.setPriority(priority);
			thread.start();
		}

	}

}
