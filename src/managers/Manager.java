package managers;

public interface Manager {
	
	public void waitUntilDone();
	public String getState();
	public void start();
	public void start(int priority);
	
}
