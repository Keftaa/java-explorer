package sandbox;

import gui.Constants;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Label;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Main {
	public static void main(String[] args) {
		Label label1 = new Label("huehue");
		Label label2 = new Label("hahaha");
		JButton button = new JButton("Button1");
		JButton button2 = new JButton("Button2");
		JFrame jframe = new JFrame();
		JPanel panel = new JPanel();
		jframe.add(panel);
		GridBagLayout layout = new GridBagLayout();
		panel.setLayout(layout);
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		GridBagConstraints gbc2 = new GridBagConstraints();
		gbc2.gridx = 1;
		gbc2.gridy = 0;
		gbc.insets = new Insets(16, 16, 16, 16);
		panel.add(button, gbc);
		panel.add(button2, gbc2);
		layout.addLayoutComponent(button, gbc);
		jframe.setSize(Constants.WIDTH, Constants.HEIGHT);
		jframe.setResizable(false);
		jframe.setVisible(true);
	}

}
