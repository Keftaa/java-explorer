package sandbox;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileSystemView;

import static java.nio.file.StandardCopyOption.*;
public class BorderLayoutTest {
	JFrame frame;
	JPanel panel, top, bottom, left, right, center;
	BorderLayout borderLayout;
	JButton bEdit, bCopy, bMove, bDelete, bSearch, bCreateFolder, bSortName,
			bSortExtension, bSortDate, bSortSize;
	GridBagConstraints gridConstraints;
	JList<File> leftList;
	JList<String> rightList;
	JScrollPane scrollPane;
	JList<File> centerList;
	JPanel leftPanel, centerPanel, rightPanel;

	BorderLayoutTest() {
		frame = new JFrame();
		panel = new JPanel();
		leftPanel = new JPanel();
		leftPanel.setBackground(Color.ORANGE);
		centerPanel = new JPanel();
		centerPanel.setBackground(Color.GREEN);
		rightPanel = new JPanel();
		rightPanel.setBackground(Color.PINK);
		borderLayout = new BorderLayout();
		panel.setLayout(borderLayout);
		bEdit = new JButton("Edit");
		bCopy = new JButton("Copy");
		bMove = new JButton("Move");
		bDelete = new JButton("Delete");
		bSearch = new JButton("Search");
		bCreateFolder = new JButton("Create Folder");
		top = new JPanel(new GridBagLayout());
		top.setBackground(Color.BLACK);
		top.setPreferredSize(new Dimension(800, 80));
		gridConstraints = new GridBagConstraints();
		gridConstraints.insets = new Insets(5, 0, 5, 5);
		gridConstraints.gridx = 0;
		gridConstraints.gridy = 0;
		gridConstraints.gridwidth = 6;
		top.add(new JLabel("Java Explorer"), gridConstraints);
		gridConstraints.gridx = 0;
		gridConstraints.gridy = 1;
		gridConstraints.gridwidth = 1;
		top.add(bEdit, gridConstraints);
		gridConstraints.gridx = 1;
		gridConstraints.gridy = 1;
		gridConstraints.gridwidth = 1;
		top.add(bCopy, gridConstraints);
		gridConstraints.gridx = 2;
		gridConstraints.gridy = 1;
		gridConstraints.gridwidth = 1;
		top.add(bMove, gridConstraints);
		gridConstraints.gridx = 3;
		gridConstraints.gridy = 1;
		gridConstraints.gridwidth = 1;
		top.add(bDelete, gridConstraints);
		gridConstraints.gridx = 4;
		gridConstraints.gridy = 1;
		gridConstraints.gridwidth = 1;
		top.add(bSearch, gridConstraints);
		gridConstraints.gridx = 5;
		gridConstraints.gridy = 1;
		gridConstraints.gridwidth = 1;
		top.add(bCreateFolder, gridConstraints);

		bottom = new JPanel();
		bottom.setBackground(Color.BLUE);

		left = new JPanel();
		left.setBackground(Color.YELLOW);

		leftList = new JList<File>(File.listRoots());
		left.add(leftList);
		try {
			Files.copy(Paths.get("/home/adnan/test/lol.txt"), Paths.get("/home/adnan/test/lol1.txt"), REPLACE_EXISTING);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		right = new JPanel();
		right.setBackground(Color.GREEN);

		center = new JPanel(new GridBagLayout());
		center.setBackground(Color.RED);

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridheight = 3;
		gbc.insets = new Insets(5, 5, 10, 5);
		gbc.fill = GridBagConstraints.BOTH;

		centerPanel = new JPanel();
		centerPanel.setBackground(Color.PINK);

		File dir = new File("/");
		File[] filesList = dir.listFiles();
		String[] data = { "feafaef", "feafaef", "feafaef", "feafaef",
				"feafaef", "feafaef", "feafaef", "feafaef", "feafaef",
				"feafaef", "feafaef", "feafaef", "feafaef", "feafaef",
				"feafaef", "feafaef", "feafaef", "feafaef", "feafaef",
				"feafaef" };
		centerList = new JList<File>(filesList);
		MyCellRenderer cellRenderer = new MyCellRenderer();
		centerList.setCellRenderer(cellRenderer);
		centerList.setPreferredSize(new Dimension(300, centerPanel.getHeight()/2));
		scrollPane = new JScrollPane();
		scrollPane.setViewportView(centerList);

		centerList.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {

					final File dir = centerList.getSelectedValue();
					if (dir.canRead() && !dir.isFile()) {
						final File[] filesList = dir.listFiles();
						centerList.setListData(filesList);
						
					}
						if (dir.isFile()) {
							System.out.println("isFileTrue");
							if (!Desktop.isDesktopSupported())
								System.out.println("Desktop is not supported!");
							SwingUtilities.invokeLater(new Runnable(){

								@Override
								public void run() {
									try {
										Desktop.getDesktop().open(dir);
									} catch (IOException e) {
										e.printStackTrace();
									}
									
								}
								
							});
	
						}
					}
				if (arg0.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
					final File dir = centerList.getSelectedValue();
					final File prev = dir.getParentFile().getParentFile();
					final File[] filesList = prev.listFiles();
					centerList.setListData(filesList);
				}
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyTyped(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

		});

		centerPanel.add(scrollPane);
		center.add(centerPanel, gbc);

		gbc.gridx = GridBagConstraints.RELATIVE;
		gbc.gridy = 0;
		gbc.gridwidth = 2;
		gbc.gridheight = 2;

		rightPanel = new JPanel();
		rightPanel.setBackground(Color.CYAN);
		rightList = new JList<String>(data);
		rightList.setPreferredSize(new Dimension(400, 400));
		rightPanel.add(rightList);
		center.add(rightList, gbc);

		gbc.gridx = 1;
		gbc.gridy = GridBagConstraints.RELATIVE;
		gbc.gridwidth = 1;
		bSortDate = new JButton("Sort By Date");
		center.add(bSortDate, gbc);

		bSortExtension = new JButton("Sort by Extension");
		center.add(bSortExtension, gbc);

		gbc.gridx = 2;
		bSortName = new JButton("Sort by Name");
		center.add(bSortName, gbc);

		bSortSize = new JButton("Sort by Size");
		center.add(bSortSize, gbc);

		panel.add(left, BorderLayout.LINE_START);
		panel.add(center, BorderLayout.CENTER);
		panel.add(right, BorderLayout.LINE_END);
		panel.add(top, BorderLayout.PAGE_START);
		panel.add(bottom, BorderLayout.PAGE_END);
		frame.getContentPane().add(panel);
		frame.setSize(800, 640);
		frame.setResizable(false);
		frame.setVisible(true);

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable(){

			@Override
			public void run() {
				new BorderLayoutTest();
				
			}
			
		});
	}
}

class MyCellRenderer extends JLabel implements ListCellRenderer{
	FileSystemView fileSystemView;
	MyCellRenderer(){
		fileSystemView = FileSystemView.getFileSystemView();
	}
	@Override
	public Component getListCellRendererComponent(JList arg0, Object arg1,
			int arg2, boolean arg3, boolean arg4) {
		// TODO Auto-generated method stub
		JLabel label = new JLabel();
		label.setOpaque(true);
		File file = (File) arg1;
        label.setIcon(fileSystemView.getSystemIcon(file));
        label.setText(fileSystemView.getSystemDisplayName(file));
        if (arg3) {
            label.setBackground(Color.BLACK);
            label.setForeground(Color.BLUE);
        } else {
            label.setBackground(Color.WHITE);
            label.setForeground(Color.BLACK);
        }
		return label;
	}
	
}