package views;

import gui.Constants;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.io.File;

import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.filechooser.FileSystemView;

import models.MyCellRenderer;

public class CenterPanel extends View {
	private JList<File> filesList;
	private JScrollPane scrollPane;
	private MyCellRenderer renderer;

	public CenterPanel() {
		super();
		filesList = new JList<File>();
		filesList.setLayoutOrientation(JList.HORIZONTAL_WRAP);
		renderer = new MyCellRenderer();
		registerListRenderer();
		scrollPane = new JScrollPane();
		scrollPane.setViewportView(filesList);
		scrollPane.setName("filesListPane");
		rootPanel.add(scrollPane);
		rootPanel.setBackground(Color.BLACK);
	}

	private void registerListRenderer() {
		filesList.setCellRenderer(renderer);
	}

	@Override
	public Component getMainComponent() {
		return filesList;
	}

	@Override
	public void updateDefaultSize(){
		rootPanel.setMinimumSize(new Dimension(Constants.WIDTH/2, Constants.HEIGHT));
		scrollPane.setMinimumSize(new Dimension(rootPanel.getMinimumSize().width
				- rootPanel.getMinimumSize().width/4,
				rootPanel.getMinimumSize().height - rootPanel.getMinimumSize().height/6));
		scrollPane.setPreferredSize(new Dimension(rootPanel.getMinimumSize().width
				- rootPanel.getMinimumSize().width/4,
				rootPanel.getMinimumSize().height - rootPanel.getMinimumSize().height/6));	
	}

}