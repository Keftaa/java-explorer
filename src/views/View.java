package views;

import gui.Constants;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JPanel;

public class View {
	protected JPanel rootPanel;
	protected GridBagConstraints gbc;

	protected View(){
		rootPanel = new JPanel(new GridBagLayout());
		gbc = new GridBagConstraints();
	}
	
	public List<Component> getComponentsByType(Class<?> clazz) throws InstantiationException, IllegalAccessException {
		List<Component> results = new ArrayList<Component>(); 
		Component[] components = rootPanel.getComponents();
		for (Component c : components) {
			if (c.getClass().getName().equals(clazz.newInstance().getClass().getName()))
				results.add(c);

		}
		return results;
		
	}


	public Component getComponentByName(String name) {
		Component[] components = rootPanel.getComponents();
		for(Component c: components)
			if(c.getName()!= null && c.getName().equals(name))
				return c;
		return null;
		
	}
	
	public JPanel getPanel(){
		return rootPanel;
		
	}
	
	public Component getMainComponent(){
		return null;
	}
	
	public void updateSize(int width, int height){
		rootPanel.setMinimumSize(new Dimension(width, height));
	}
	public void updateDefaultSize(){}

}
