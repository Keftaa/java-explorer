package views;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import gui.Constants;

import javax.swing.JFrame;
import javax.swing.JPanel;
import linkers.CopyLinker;
import linkers.DeleteLinker;
import linkers.FilesListLinker;
import linkers.FilesTableLinker;
import linkers.Linker;
import linkers.LinkersManager;
import linkers.MoveLinker;
import linkers.RenameLinker;
import linkers.RootsListLinker;
import linkers.SizeLinker;

public class MainView extends View {
	private JFrame frame;
	private List<View> views;
	public TopPanel topPanel;
	public CenterPanel centerPanel;
	public LeftPanel leftPanel;
	public RightPanel rightPanel;

	public MainView() {
		topPanel = new TopPanel();
		centerPanel = new CenterPanel();
		leftPanel = new LeftPanel();
		rightPanel = new RightPanel();
		frame = new JFrame();
		rootPanel = new JPanel(new BorderLayout(10, 10));
		rootPanel.add(topPanel.getPanel(), BorderLayout.PAGE_START);
		rootPanel.add(centerPanel.getPanel(), BorderLayout.CENTER);
		rootPanel.add(leftPanel.getPanel(), BorderLayout.LINE_START);
		rootPanel.add(rightPanel.getPanel(), BorderLayout.LINE_END);
		
		frame.getContentPane().add(rootPanel);
		frame.setSize(Constants.WIDTH, Constants.HEIGHT);
		frame.setResizable(true);
		frame.setVisible(true);

	}
	
	public List<View> getViews(){
		if(views==null){
			views = new ArrayList<View>();
			views.add(topPanel);
			views.add(centerPanel);
			views.add(leftPanel);
			views.add(rightPanel);
		}
		return views;
	}
	
	

	@Override
	public Component getMainComponent() {
		return frame;
	}



	public static void main(String[] args) {
		Thread mainThread = new Thread(new Runnable() {

			@Override
			public void run() {
				MainView view = new MainView();
				LinkersManager lm = new LinkersManager();
				CopyLinker c = new CopyLinker(view.topPanel, view.centerPanel);
				MoveLinker l = new MoveLinker(view.topPanel, view.centerPanel);
				DeleteLinker d = new DeleteLinker(view.topPanel,
						view.centerPanel);
				RenameLinker r = new RenameLinker(view.topPanel,
						view.centerPanel);
				RootsListLinker rl = new RootsListLinker(view.leftPanel);
				SizeLinker sl = new SizeLinker(view, view.getViews());
				
				c.addObserver(lm);
				l.addObserver(lm);
				d.addObserver(lm);
				r.addObserver(lm);
				c.init();
				l.init();
				d.init();
				r.init();
				rl.init();
				sl.init();
				FilesListLinker fl = new FilesListLinker(view.centerPanel,
						lm.getObservables());
				fl.addObserver(lm);
				fl.init();
				System.out.println(lm.getObservables());
				FilesTableLinker ft = new FilesTableLinker(view.rightPanel, lm.getObservables());
				ft.init();
				c.addObserver(fl);
				l.addObserver(fl);
				d.addObserver(fl);
				r.addObserver(fl);
				rl.addObserver(fl);
				fl.addObserver(ft);

			}
		});

		mainThread.start();
	}
}
