package views;

import gui.Constants;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.Panel;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import json.JsonHelper;

import org.json.JSONArray;
import org.json.JSONObject;

public class TopPanel extends View {
	private JLabel title;
	private List<JButton> buttons;
	TopPanel(){
		super();
		title = JsonHelper.getLabel("topPanelLabels", "title");
		buttons = JsonHelper.getButtons("topPanelButtons");
		gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = buttons.size();
		rootPanel.add(title, gbc);
		
		for(JButton b: buttons){
			gbc = new GridBagConstraints();
			gbc.gridy = 1;
			gbc.gridx = GridBagConstraints.RELATIVE;
			rootPanel.add(b, gbc);
		}
	}

}
