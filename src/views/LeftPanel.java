package views;

import gui.Constants;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.io.File;

import javax.swing.JList;
import javax.swing.JScrollPane;

import models.MyCellRenderer;

public class LeftPanel extends View {
	private JList<File> rootsList;
	private JScrollPane scrollPane;
	private MyCellRenderer renderer;
	public LeftPanel(){
		super();
		rootsList = new JList<File>();
		renderer = new MyCellRenderer();
		registerListRenderer();
		scrollPane = new JScrollPane();
		scrollPane.setViewportView(rootsList);
		scrollPane.setName("rootsListPane");
		rootPanel.add(scrollPane);
		rootPanel.setBackground(Color.BLUE);
		rootPanel.setMinimumSize(new Dimension(Constants.WIDTH/8, Constants.HEIGHT));
		scrollPane.setPreferredSize(rootPanel.getMinimumSize());
	}
	
	@Override
	public void updateDefaultSize(){
		rootPanel.setMinimumSize(new Dimension(Constants.WIDTH/8, Constants.HEIGHT));
		scrollPane.setMinimumSize(rootPanel.getMinimumSize());
		scrollPane.setPreferredSize(rootPanel.getMinimumSize());	
	}
	
	private void registerListRenderer(){
		rootsList.setCellRenderer(renderer);
	}
	
	@Override
	public Component getMainComponent(){
		return rootsList;
	}
}
