package views;

import gui.Constants;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.io.File;

import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;

public class RightPanel extends View {
	private JTable filesTable;
	private JScrollPane scrollPane;
	
	public RightPanel(){
		super();
		filesTable = new JTable();
		scrollPane = new JScrollPane();
		scrollPane.setViewportView(filesTable);
		scrollPane.setName("filesTablePane");
		rootPanel.add(scrollPane);
		rootPanel.setBackground(Color.GREEN);
	}
	
	
	@Override
	public Component getMainComponent(){
		return filesTable;
	}
	
	@Override
	public void updateDefaultSize(){
		rootPanel.setMinimumSize(new Dimension(Constants.WIDTH*13/24, Constants.HEIGHT));
		scrollPane.setMinimumSize(rootPanel.getMinimumSize());
		scrollPane.setPreferredSize(rootPanel.getMinimumSize());	
	}
	
	
	
}

