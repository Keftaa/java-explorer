package json;

import gui.Constants;

import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import models.MyTableModel;

import org.json.JSONArray;
import org.json.JSONObject;

public class JsonHelper {
	private static File file;
	private static  byte[] encoded;
	private static String json;
	private static JSONObject obj;
	private static JSONArray array;
	
	public static List<JButton> getButtons(String identifier){
		List<JButton> buttons = new ArrayList<JButton>();
		file = new File(Constants.APP_PATH + "/src/json/buttons.json");
		try {
			encoded = Files.readAllBytes(Paths.get(file.getAbsolutePath()));
		} catch (IOException e) {
			e.printStackTrace();
		}	
		try {
			json = new String(encoded, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		obj = new JSONObject(json);
		array = obj.getJSONArray(identifier);
		
		for (int i = 0; i < array.length(); i++) {
			JSONObject jsonObject = array.getJSONObject(i);
			JButton button = new JButton();
			button.setName(jsonObject.getString("label"));
			button.setText(jsonObject.getString("label"));
			buttons.add(button);
		}
		
		return buttons;
		
	}
	
	public static JLabel getLabel(String arrayIdentifier, String labelIdentifier){
		file = new File(Constants.APP_PATH + "/src/json/labels.json");
		try {
			encoded = Files.readAllBytes(Paths.get(file.getAbsolutePath()));
		} catch (IOException e) {
			e.printStackTrace();
		}	
		try {
			json = new String(encoded, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		obj = new JSONObject(json);
		array = obj.getJSONArray(arrayIdentifier);
		
		for (int i = 0; i < array.length(); i++) {
			JSONObject jsonObject = array.getJSONObject(i);
			if(!jsonObject.getString("id").equals(labelIdentifier)) continue;
			
			JLabel label = new JLabel();
			label.setName(jsonObject.getString("id"));
			label.setText(jsonObject.getString("label"));
			float red = (float) jsonObject.getDouble("r");
			float green = (float) jsonObject.getDouble("g");
			float blue = (float) jsonObject.getDouble("b");
			label.setForeground(new Color(red, green, blue));
			Font labelFont = label.getFont();
			label.setFont(new Font(labelFont.getName(), Font.PLAIN, jsonObject.getInt("fontSize")));
			return label;
		}
		return null;		
	}

	public static MyTableModel getTable(){
		MyTableModel model = new MyTableModel();
		file = new File(Constants.APP_PATH + "/src/json/table.json");
		try {
			encoded = Files.readAllBytes(Paths.get(file.getAbsolutePath()));
		} catch (IOException e) {
			e.printStackTrace();
		}	
		try {
			json = new String(encoded, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		obj = new JSONObject(json);
		array = obj.getJSONArray("jtable");
		String[] columnNames = new String[array.length()];
		for (int i = 0; i < array.length(); i++) {
			JSONObject jsonObject = array.getJSONObject(i);
			columnNames[i] = jsonObject.getString("name");
		}
		
		model.setColumnNames(columnNames);
		
		return model;
		
	}

}
